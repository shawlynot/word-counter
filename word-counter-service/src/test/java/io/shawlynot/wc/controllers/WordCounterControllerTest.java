package io.shawlynot.wc.controllers;

import io.shawlynot.wc.Application;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = Application.class
)
class WordCounterControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private String wordCounterUrl;

    @BeforeEach
    void setUp(){
        wordCounterUrl = "http://localhost:" + port + "/wordcounter";
    }

    @Test
    void addWordSuccessfully() {
        assertEquals(
                HttpStatus.ACCEPTED,
                restTemplate.postForEntity(wordCounterUrl, "horse", Object.class).getStatusCode()
        );
        assertEquals(
                "1",
                restTemplate.getForObject(wordCounterUrl + "/horse", String.class)
        );
    }

    @Test
    void badRequestForInvalidWords() {
        assertEquals(
                HttpStatus.BAD_REQUEST,
                restTemplate.postForEntity(wordCounterUrl, "invalid_w0rd", Object.class).getStatusCode()
        );
    }
}