package io.shawlynot.wc.controllers;

import io.shawlynot.wc.WordCounter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class WordCounterController {

    private final WordCounter wordCounter;

    public WordCounterController(WordCounter wordCounter) {
        this.wordCounter = wordCounter;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(IllegalArgumentException.class)
    public void invalidRequest(){
    }

    @PostMapping("/wordcounter")
    public ResponseEntity<?> addWord(@RequestBody String string){
        wordCounter.addWord(string);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @GetMapping("/wordcounter/{word}")
    public Integer countWord(@PathVariable String word){
        return wordCounter.countWord(word);
    }
}
