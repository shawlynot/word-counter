package io.shawlynot.wc.config;

import io.shawlynot.wc.InMemoryWordCounter;
import io.shawlynot.wc.WordCounter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WordCounterConfig {

    @Bean
    public WordCounter wordCounter(){
        return new InMemoryWordCounter();
    }
}
