package io.shawlynot.wc;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class InMemoryWordCounterTest {

    private InMemoryWordCounter wordCounter;

    @BeforeEach
    void setUp() {
        wordCounter = new InMemoryWordCounter();
    }

    @Test
    void addsAndCountsWords() {
        wordCounter.addWord("horse");
        wordCounter.addWord("Horse");
        wordCounter.addWord("cheese");

        assertEquals(2, wordCounter.countWord("horse"));
        assertEquals(2, wordCounter.countWord("Horse"));
        assertEquals(1, wordCounter.countWord("cheese"));
        assertEquals(0, wordCounter.countWord("other"));
    }

    @Test
    void differentLanguageSameWord() {
        wordCounter.addWord("flower");
        wordCounter.addWord("flor");
        wordCounter.addWord("blume");

        assertEquals(3, wordCounter.countWord("flower"));
        assertEquals(3, wordCounter.countWord("flor"));
    }

    @Test
    void rejectsNonAlphabeticCharacters() {
        assertThrows(IllegalArgumentException.class, () -> wordCounter.addWord("w0rd"));
        assertEquals(0, wordCounter.countWord("w0rd"));
    }

    @Test
    void rejectsNull(){
        assertThrows(IllegalArgumentException.class, () -> wordCounter.addWord(null));
    }
}