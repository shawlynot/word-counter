package io.shawlynot.wc;

import java.util.List;

/**
 * Dummy Translator class that will only translate lowercase "flower"
 */
public class DummyTranslator implements Translator {

    private final List<String> flower = List.of("flower", "flor", "blume");

    @Override
    public String translate(String word) {
        if (flower.contains(word)){
            return "flower";
        }
        return word;
    }
}
