package io.shawlynot.wc;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Maintains an in-memory map of words and their counts.
 */
public class InMemoryWordCounter implements WordCounter {

    // Avoid having to create a new AtomicInteger on every count of a missing word
    private final static AtomicInteger defaultCount = new AtomicInteger();

    private final Translator translator;
    private final Map<String, AtomicInteger> wordCount;

    public InMemoryWordCounter(Translator translator) {
        this.translator = translator;
        wordCount = new ConcurrentHashMap<>();
    }

    /**
     * Uses a dummy Translator implementation
     */
    public InMemoryWordCounter() {
        this(new DummyTranslator());
    }

    /**
     * Add a word to the in-memory map. Words are automatically translated to English and lowercased before insertion.
     * @param word a word to be counted
     * @throws IllegalArgumentException if word does not consist of only Latin alphabet characters
     */
    @Override
    public void addWord(String word) {
        if (word == null || !word.matches("([a-zA-Z])+")){
            throw new IllegalArgumentException("Word must be non-empty and contain on Latin alphabet characters");
        }
        String wordInEnglish = translator.translate(word).toLowerCase(Locale.ROOT);
        wordCount.computeIfAbsent(wordInEnglish, k -> new AtomicInteger());
        wordCount.get(wordInEnglish).incrementAndGet();
    }

    /**
     * Query for a word in the in-memory map. Words are automatically translated to English and lowercased before querying.
     * @return The number of times the word has been accepted
     */
    @Override
    public int countWord(String word) {
        String wordInEnglish = translator.translate(word).toLowerCase(Locale.ROOT);
        return wordCount.getOrDefault(wordInEnglish, defaultCount).get();
    }
}
