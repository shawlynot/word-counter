package io.shawlynot.wc;

/**
 * A translator to English for use in a WordCounter
 */
public interface Translator {

    /**
     * Translate a word to English. No guarantees are made about the case of the input or output.
     * @param word a word
     * @return the English translation of the word
     */
    String translate(String word);
}
