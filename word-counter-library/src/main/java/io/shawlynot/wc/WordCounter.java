package io.shawlynot.wc;

/**
 * Accepts words and counts the number of times the word was accepted
 */
public interface WordCounter {

    /**
     * Adds a word
     */
    void addWord(String word);

    /**
     * Returns the number of times a word was accepted
     */
    int countWord(String word);
}
